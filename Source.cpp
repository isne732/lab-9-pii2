#include <iostream>
#include <iomanip>

using namespace std;


int main()
{
	char answer;
	int turn = 0, //swap player
		board[9] = { 0 }, //initial board to 0
		round = 0; //have 9 boxes--> 9 rounds

	cout << "/////////Wellcome to TicTacToe Game!!/////////" << endl;
	cout << "          -----------------" << endl;
	cout << "          | 7 | | 8 | | 9 |" << endl;
	cout << "          -----------------" << endl;
	cout << "          | 4 | | 5 | | 6 |" << endl;
	cout << "          -----------------" << endl;
	cout << "          | 1 | | 2 | | 3 |" << endl;
	cout << "          -----------------" << endl;
	cout << "     Player = X  and  AI = 0" << endl;

	cout << endl << "Do you want to go first (y/n): "; //y --> yes / n--> no
	cin >> answer;
	while (answer != 'y' && answer != 'n') //check if user put y or other element.
	{
		cout << "Please input only 'y' or 'n', input again: ";
		cin >> answer;
	}
	if (answer == 'y')
	{
		turn = 1; //1 for player
	}
	else if (answer == 'n')
	{
		turn = 2; //2 for AI
	}

	while (true)
	{
		if (turn == 1) //player
		{
			cout << endl << "Player's turn" << endl;
			player_turn(board);
			draw_grid(board);
			turn = 2;
			round++;
		}
		else if (turn == 2) //AI
		{
			cout << endl << "AI's turn" << endl;
			ai_turn(board);
			draw_grid(board);
			turn = 1;
			round++;
		}
		if (check_win(board) == 1) //-1 for lose
		{
			cout << "YOU LOSE!!" << endl;
			break;
		}
		else if (check_win(board) == -1) //1 for win
		{
			cout << "CONGRATUATIONS!!! YOU WIN" << endl;
			break;
		}
		else if (round == 9)//if each player play every round but no one win or lose, it will draw
		{
			cout << "-DRAW-" << endl;
			break;
		}

	}
	
}

char position(int character) //assign number for characters
{
	switch (character)
	{
	case -1:
		return 'X';
	case 0:
		return ' ';
	case 1:
		return 'O';
	}
}

void draw_grid(int board[9]) //output the board
{
	cout << endl;
	cout << "        -----------------" << endl;
	cout << setw(10) << "| " << position(board[6]) << " |" << " | " << position(board[7]) << " |" << " | " << position(board[8]) << " |" << endl;
	cout << "        -----------------" << endl;
	cout << setw(10) << "| " << position(board[3]) << " |" << " | " << position(board[4]) << " |" << " | " << position(board[5]) << " |" << endl;
	cout << "        -----------------" << endl;
	cout << setw(10) << "| " << position(board[0]) << " |" << " | " << position(board[1]) << " |" << " | " << position(board[2]) << " |" << endl;
	cout << "        -----------------" << endl;
	cout << endl;
}

void player_turn(int board[9])
{
	int number; //select number from the board
	cout << "Enter the number of your position:";
	cin >> number;

	while (number < 1 || number > 9) //if player enter number that not in [1-9]
	{
		cout << "Please enter number(1-9)" << endl;
		cout << "Enter the number of your position:: ";
		cin >> number;
	}

	while (board[number - 1] != 0) //[number - 1] because array start with 0
	{
		cout << "**This position is not available**" << endl;
		cout << "Enter the number to select the position: ";
		cin >> number;
	}

	if (board[number - 1] == 0) //the position is available
	{
		board[number - 1] = -1; //output 'X'
	}
}

int check_win(int board[9])
{
	// if a player won returns 0 
	int win[8][3] = { {0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{2,4,6} };
	for (int i = 0; i < 8; ++i)
	{
		if (board[win[i][0]] == board[win[i][2]] && board[win[i][0]] != 0 && board[win[i][0]] == board[win[i][1]])
		{
			return board[win[i][2]]; //if win
		}
	}
	return 0;
}

int minimax(int board[9], int player)
{
	int winner = check_win(board);

	if (winner != 0)
		return winner * player;

	int move = -1;
	int score = -2;

	for (int i = 0; i < 9; i++) {

		if (board[i] == 0) {

			board[i] = player;
			int thisScore = -minimax(board, player * -1);

			if (thisScore > score) {
				score = thisScore;
				move = i;
			}
			board[i] = 0;//Reset board after try
		}
	}
	if (move == -1)
		return 0;

	return score;
}

void ai_turn(int board[9])
{
	int move = -1;
	int score = -2;

	for (int i = 0; i < 9; ++i)
	{
		if (board[i] == 0)
		{
			board[i] = 1;
			int tmpScore = -minimax(board, -1);
			board[i] = 0;

			if (tmpScore > score)
			{
				score = tmpScore;
				move = i;
			}

			if (board[4] == 0)
			{
				move = 4;
			}
		}
	}
	board[move] = 1;
}



